var app = angular.module("myApp", [
    'ngRoute',
    'ngMaterial',
    'ngMessages'
]);

app.config(function($routeProvider) {

    $routeProvider.when('/', {
            
            templateUrl: 'home.html'


        })
        .when('/about', {
            templateUrl: 'about.html'

        })
        .when('/apply', {
            templateUrl: 'apply.html'

        })
        .when('/intern', {
            templateUrl: 'intern.html'

        })
        .when('/portfolio', {
            templateUrl: 'portfolio.html'

        })
        .when('/services', {
            templateUrl: 'services.html'

        }).when('/contact', {
            templateUrl: 'contact.html'

        })
        .when('/blog', {
            templateUrl: 'blog.html'

        })
        .when('/career', {
            templateUrl: 'career.html'

        })
        .otherwise({
            redirectTo: '/'
        });
});

app.controller('applyCtrl', function($scope, $location) {


    $scope.apply = function() {
        $location.path('/apply');
    }

});

app.controller('internCtrl', function($scope, $location) {


    $scope.intern = function() {
        $location.path('/intern');
    }

});

app.directive('ngFooter', function() {
  return {
   
    templateUrl: 'footer.html'
  }
});

app.directive('ngHeader', function() {
  return {
   
    templateUrl: 'header.html'
  }
});


